section .text

%define EXIT_CODE 60
%define WRITE_CODE 1

%define NEWLINE_SYMBOL_CODE 10
%define MINUS_SYMBOL_CODE 45
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	    cmp byte[rdi+rax], 0
	    je .end
	    inc rax
	    jmp .loop
    .end:
	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi,rdi
    call string_length
    mov rdx, rax
    mov rdi,1
    mov rax, WRITE_CODE
    syscall 
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi,rsp
    mov rdi,1 
    mov rax, WRITE_CODE
    mov rdx,1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL_CODE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, 32
    lea r9, [rsp+ 20]
    mov rax, rdi
    mov r10, 10
    mov byte[r9+1], 0
    .loop:
        xor rdx,rdx
        div r10
        add rdx, 48
        mov [r9], dl
        cmp rax, 0
        je .end
        dec r9
        jmp .loop
    .end:
        lea rdi, [r9]
        call print_string
        add rsp, 32
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .negative
    jmp .positive
    .negative:
        push rdi
        mov rdi, 45
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
    .positive:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax,rax
    .loop:
        mov r8b, byte[rdi + rax]
        cmp r8b, byte[rsi + rax]
        je .check_zero
        jmp .not_equals
    .check_zero:
        cmp r8b, 0
        je .equals
        inc rax
        jmp .loop
    .not_equals:
        xor rax, rax
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    .sysread:
        xor rdi, rdi
        mov rdx, 1
        mov rsi, rsp 
        syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    xor r10,r10
    .read:
        call read_char
        cmp rax, 0x20
        je .skip
        cmp rax, 0x9
        je .skip
        cmp rax, 0xA
        je .skip
        mov [r8 + r10], rax
        inc r10
        cmp r10, r9
        je .check_buffer
        jmp .read
    .skip:
        cmp r10,0
        je .read
        jmp .end_success
    .check_buffer:
        call read_char
        cmp rax, 0
        je .end_success
        jmp .end_fail
    .end_success:
        mov byte[r8+r10], 0
        mov rdi, r8
        call string_length
        mov rdx,rax
        mov rax,r8
        ret
    .end_fail:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, rdi
    call string_length
    xor r9, r9
    xor r10,r10
    xor r11, r11
    cmp r9, rax
    je .end
    .loop:
        mov dl, byte[r8+r9]
        sub dl, 48
        cmp dl, 0
        jl .div
        cmp dl, 9
        jnle .div
        add r10b, dl 
        inc r9
        cmp r9, rax
        je .end
        mov r11, r10
        add r10, r10
        lea r10, [r10+4*r10]
        jmp .loop
    .div:
        mov r10, r11
        jmp .end

    .end:
        mov rdx, r9
        mov rax, r10

        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8, rdi
    mov dl, byte[r8]
    cmp dl, MINUS_SYMBOL_CODE
    je .negative
    jmp .positive

    .positive:
        jmp parse_uint
    .negative:
        inc rdi
        call parse_uint
        cmp rdx,0
        je .fail
        inc rdx
        neg rax
        ret
    .fail:
        xor rax, rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    cmp rdx, 0
    je .fail                ;If buff length = 0 -> string_length causes segmentation fault
    call string_length
    cmp rax,rdx
    jnbe .fail
    inc rax                 ;U need to cp 0 too
    xor r9,r9
    xor r10,r10
    .loop:
        mov r8b, byte[rdi + r9]
        mov byte[rsi + r9], r8b
        inc r9
        cmp rax, r9
        je .end
        jmp .loop
    .fail:
        xor rax,rax
    .end:
        ret
